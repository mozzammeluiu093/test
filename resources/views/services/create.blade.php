@extends('templates.master')

@section('content')

    <h2>Create Data</h2>
    <hr/>
    <a class="btn btn-primary" href="/services" style="margin-bottom: 15px;">Read Data</a>

    {!! Form::open(['id' => 'dataForm', 'url' => 'services']) !!}
    <div class="form-group">
        {!! Form::label('service_name', 'Service Name'); !!}
        {!! Form::text('service_name', null, ['placeholder' => 'Enter service name', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('client_name', 'Client Name'); !!}
        {!! Form::text('client_name', null, ['placeholder' => 'Enter client name', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Client Email') !!}
        {!! Form::email('email', null, ['placeholder' => 'Enter email', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('phone', 'Phone'); !!}
        {!! Form::text('phone', null, ['placeholder' => 'Enter phone number', 'class' => 'form-control']); !!}
    </div>

    <div class="form-group">
        {!! Form::label('address', 'Address'); !!}
        {!! Form::text('address', null, ['placeholder' => 'Enter address', 'class' => 'form-control']); !!}
    </div>

    {!! Form::submit('Create', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
@endsection()