@extends('templates.master')

@section('content')

    <h2>Services</h2>
    <hr/>
    <a class="btn btn-primary" href="services/create" style="margin-bottom: 15px;">Create New</a>

    @if(Session::has('message'))
    <div class="alert-custom">
        <p>{!! Session('message') !!}</p>
    </div>
    @endif()

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="padding-left: 15px;">#</th>
            <th>Service Name</th>
            <th>Client Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th width="110px;">Action</th>
        </tr>
        </thead>
        <tbody>

        @foreach($services as $service)
            <tr>
                <td style="padding-left: 15px;">{!! $service->id !!}</td>
                <td>{!! $service->service_name !!}</td>
                <td>{!! $service->client_name !!}</td>
                <td>{!! $service->email !!}</td>
                <td>{!! $service->phone !!}</td>
                <td>{!! $service->address !!}</td>
                <td>
                    <a class="btn btn-success btn-sm" href="services/{!! $service->id !!}/edit">Edit</a>

                    {!! Form::open(['id' => 'deleteForm', 'method' => 'DELETE', 'url' => '/services/' . $service->id]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection()