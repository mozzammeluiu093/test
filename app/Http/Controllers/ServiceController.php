<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\facades\Auth;
use Mail;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $services = Service::orderBy('id')->get();
        return view('services.index', ['services' => $services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $data = $request->all();
	    $data['user_id'] = Auth::user()->id;
	    Service::create($data);

        Mail::to($data['email'])->send(new SendMail());

	    Session::flash('message', $data['service_name'] . ' added successfully');
	    return redirect('/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$service = Service::find($id);
        return view('services/edit', ['service' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $service->update($data);

	    Session::flash('message', $service['service_name'] . ' updated successfully');
        return redirect('/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $service = Service::find($id);
        $service->destroy($id);

	    Session::flash('message', $service['service_name'] . ' deleted successfully');
	    return redirect('/services');
    }
}
