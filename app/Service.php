<?php
/**
 * Created by PhpStorm.
 * User: mozza
 * Date: 9/12/2018
 * Time: 2:18 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'service_name',
        'client_name',
        'email',
        'phone',
        'address',
        'service_date',
        'service_time',
        'user_id',
    ];

}
